class BattleshipGame
  attr_reader :board, :player

  def initialize(board, player)
    @board = board
    @player = player
  end

  def play_turn
    pos = player.get_play
    attack(pos)
  end

  def attack(pos)
    board.grid[pos[0]][pos[1]] = :x
  end

  def play
    until game_over?
      puts "Enter coordinates (row,col)"
      play_turn
      display_status
    end
    puts "Congratulations! You won!"
  end

  def game_over?
    board.won?
  end

  def count
    board.count
  end

  def display_status
    board.display
    puts "Number of ships remaining: #{count}"
  end

end

############
puts "Welcome to Battleship!"
# puts "Player, enter your name:"
# p1_name = gets.chomp
# p1_name = "Ana"
# player1 = HumanPlayer.new(p1_name)
#
# board = Board.new
# board.display
# game = BattleshipGame.new(board, player1)
# board.populate_grid
# puts "Let the game begin, #{p1_name}!"
#
# game.play
