class Board
  attr_reader :grid

  def self.default_grid
    @default_grid = Array.new(10) { Array.new(10) }
  end

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def display
    puts "  0 1 2 3 4 5 6 7 8 9"
    row_num = 0
    @grid.each do |row|
      print "#{row_num}"
      row.each do |col|
        if col == nil
          print "| "
        else
          print "|#{col}"
        end
      end
      puts "|"
      row_num += 1
    end
  end

  def count(marker = :s)
    num = 0
    @grid.each do |row|
      row.each do |col|
        num += 1 if col == marker
      end
    end
    num
  end

  def empty?(pos = nil)
    if pos
      return !@grid[pos[0]][pos[1]]
    end
    count == 0? true : false
  end

  def full?
    count(nil) == 0? true : false
  end

  def place_random_ship
    raise "Board is full!" if full?

    x_coord, y_coord = rand(@grid.length), rand(@grid.length)

    if @grid[x_coord][y_coord] == nil
      @grid[x_coord][y_coord] = :s
    else
      place_random_ship
    end
  end

  def won?
    empty?
  end

  def populate_grid
    10.times {place_random_ship}
  end

  def [](x_coord=1, y_coord=1)
    @grid[x_coord][y_coord]
  end

end
